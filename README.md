
Скрипт с решением называется task.py

```python 
# Задача 1. 
# Даны два списка, нужно вернуть элементы, которые есть в 1-ом списке, но нет во 2-ом. 
# Оценить эффективность своего решения.

def get_unique_items(first_enum: list, second_enum: list) -> list:
    return list(set(first_enum) - set(second_enum))

# get_unique_items([1,2,3,4,5,6], [3,6])
# >>> [1, 2, 4, 5]

# Данное решение эффективнее по сравнению с методом перебора


# Задача 2. 
# Дан массив целых чисел. Нужно удалить из него нули. 
# Можно использовать только О(1) дополнительной памяти.
def remove_zero(numbers: list) -> list:
    numbers_set = set(numbers)
    if 0 in numbers_set:
        numbers_set.remove(0)
    
    return list(numbers_set)

# remove_zero([1,3,0,4,0,5,0,6,4])
# >>> [1, 3, 4, 5, 6]
```
